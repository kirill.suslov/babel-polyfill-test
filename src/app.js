import express from 'express';

const app = express();
const PORT = process.env.PORT || 3006;

app.get('*', (req, res)=> {
  const app = req.url;
  res.send(app);
});

app.listen(PORT, ()=> {
  console.log('😎 Server is listening on http://localhost:' + PORT);
});
